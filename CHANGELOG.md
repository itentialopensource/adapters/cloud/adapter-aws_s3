
## 0.9.7 [11-11-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-aws_s3!32

---

## 0.9.6 [10-15-2024]

* Changes made at 2024.10.14_21:24PM

See merge request itentialopensource/adapters/adapter-aws_s3!31

---

## 0.9.5 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-aws_s3!29

---

## 0.9.4 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-aws_s3!28

---

## 0.9.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aws_s3!27

---

## 0.9.2 [08-14-2024]

* Changes made at 2024.08.14_19:41PM

See merge request itentialopensource/adapters/adapter-aws_s3!26

---

## 0.9.1 [08-07-2024]

* Changes made at 2024.08.06_21:45PM

See merge request itentialopensource/adapters/adapter-aws_s3!25

---

## 0.9.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!24

---

## 0.8.5 [03-28-2024]

* Changes made at 2024.03.28_13:27PM

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!22

---

## 0.8.4 [03-11-2024]

* Changes made at 2024.03.11_15:38PM

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!21

---

## 0.8.3 [02-28-2024]

* Changes made at 2024.02.28_11:54AM

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!20

---

## 0.8.2 [01-27-2024]

* Added dynamic region support

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!19

---

## 0.8.1 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!18

---

## 0.8.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!13

---

## 0.7.0 [11-08-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!13

---

## 0.6.4 [09-29-2023]

* Patch/fix put object

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!14

---

## 0.6.3 [08-01-2023]

* Bug fixes and performance improvements

See commit f8ea7c6

---

## 0.6.2 [05-30-2023]

* updated request to use http.request due to security vulnerability

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!11

---

## 0.6.1 [04-25-2023]

* Patch/fix uripath

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!10

---

## 0.6.0 [04-06-2023]

* Work on adding dynamic buckets to the host

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!8

---

## 0.5.0 [12-06-2022]

* Work on adding dynamic buckets to the host

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!8

---

## 0.4.1 [07-14-2022]

* patch/touchup - touch ups to adapter.js

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!7

---

## 0.4.0 [07-13-2022]

- Migration to the latest Adapter Foundation
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!6

---

## 0.3.4 [03-02-2021]

- Migration to bring up to the latest foundation

- Script handles
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity

- Manual Work
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!5

---

## 0.3.3 [07-06-2020] & 0.3.2 [07-06-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!4

---

## 0.3.1 [01-16-2020]

- Update the adapter to the latest foundation. Also added AWS authentication

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!3

---

## 0.3.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!2

---

## 0.2.0 [09-12-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-aws_s3!1

---

## 0.1.2 [08-14-2019] & 0.1.1 [08-14-2019]

- Initial Commit

See commit ed77ad1

---
