/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-aws_s3',
      type: 'Awss3',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Awss3 = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Awss3 Adapter Test', () => {
  describe('Awss3 Class Tests', () => {
    const a = new Awss3(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let bucketBucket = 'fakedata';
    let bucketKey = 'fakedata';
    const bucketUploadId = 'fakedata';
    const bucketCompleteMultipartUploadBodyParam = {
      CompleteMultipartUpload: {
        Parts: [
          {
            ETag: 'string',
            PartNumber: 5
          }
        ]
      }
    };
    describe('#completeMultipartUpload - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.completeMultipartUpload(bucketBucket, bucketKey, bucketUploadId, bucketCompleteMultipartUploadBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.Location);
                assert.equal('string', data.response.bucket);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.ETag);
              } else {
                runCommonAsserts(data, error);
              }
              bucketBucket = data.response.bucket;
              bucketKey = data.response.key;
              saveMockData('Bucket', 'completeMultipartUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketCreateBucketBodyParam = {
      CreateBucketConfiguration: {
        LocationConstraint: 'cn-north-1'
      }
    };
    describe('#createBucket - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBucket(bucketBucket, bucketCreateBucketBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'createBucket', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listObjects(bucketBucket, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.IsTruncated);
                assert.equal('string', data.response.markerQuery);
                assert.equal('string', data.response.NextMarker);
                assert.equal(true, Array.isArray(data.response.Contents));
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Prefix);
                assert.equal('string', data.response.Delimiter);
                assert.equal(6, data.response.maxKeysQuery);
                assert.equal(true, Array.isArray(data.response.CommonPrefixes));
                assert.equal('url', data.response.EncodingType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'listObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#headBucket - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.headBucket(bucketBucket, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'headBucket', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketCopyObjectBodyParam = {
      'x-amz-meta-': {}
    };
    describe('#copyObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.copyObject(bucketBucket, bucketKey, bucketCopyObjectBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.CopyObjectResult);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'copyObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listParts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listParts(bucketBucket, bucketKey, null, null, bucketUploadId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.bucket);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.UploadId);
                assert.equal(1, data.response.partNumberMarkerQuery);
                assert.equal(1, data.response.NextPartNumberMarker);
                assert.equal(10, data.response.maxPartsQuery);
                assert.equal(true, data.response.IsTruncated);
                assert.equal(true, Array.isArray(data.response.Parts));
                assert.equal('object', typeof data.response.Initiator);
                assert.equal('object', typeof data.response.Owner);
                assert.equal('STANDARD', data.response.StorageClass);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'listParts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#headObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.headObject(bucketBucket, bucketKey, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.Metadata);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'headObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketAnalyticsIdBucket = 'fakedata';
    const bucketAnalyticsIdId = 'fakedata';
    const bucketAnalyticsIdAnalytics = true;
    const bucketAnalyticsIdPutBucketAnalyticsConfigurationBodyParam = {
      AnalyticsConfiguration: {}
    };
    describe('#putBucketAnalyticsConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketAnalyticsConfiguration(bucketAnalyticsIdBucket, bucketAnalyticsIdId, bucketAnalyticsIdPutBucketAnalyticsConfigurationBodyParam, bucketAnalyticsIdAnalytics, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAnalyticsId', 'putBucketAnalyticsConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketAnalyticsConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketAnalyticsConfiguration(bucketAnalyticsIdBucket, bucketAnalyticsIdId, bucketAnalyticsIdAnalytics, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.AnalyticsConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAnalyticsId', 'getBucketAnalyticsConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketCorsBucket = 'fakedata';
    const bucketCorsCors = true;
    const bucketCorsPutBucketCorsBodyParam = {
      CORSConfiguration: {}
    };
    describe('#putBucketCors - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketCors(bucketCorsBucket, bucketCorsPutBucketCorsBodyParam, bucketCorsCors, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketCors', 'putBucketCors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketCors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketCors(bucketCorsBucket, bucketCorsCors, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.CORSRules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketCors', 'getBucketCors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketEncryptionBucket = 'fakedata';
    const bucketEncryptionEncryption = true;
    const bucketEncryptionPutBucketEncryptionBodyParam = {
      ServerSideEncryptionConfiguration: {}
    };
    describe('#putBucketEncryption - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketEncryption(bucketEncryptionBucket, bucketEncryptionPutBucketEncryptionBodyParam, bucketEncryptionEncryption, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketEncryption', 'putBucketEncryption', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketEncryption - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketEncryption(bucketEncryptionBucket, bucketEncryptionEncryption, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.ServerSideEncryptionConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketEncryption', 'getBucketEncryption', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketInventoryIdBucket = 'fakedata';
    const bucketInventoryIdId = 'fakedata';
    const bucketInventoryIdInventory = true;
    const bucketInventoryIdPutBucketInventoryConfigurationBodyParam = {
      InventoryConfiguration: {}
    };
    describe('#putBucketInventoryConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketInventoryConfiguration(bucketInventoryIdBucket, bucketInventoryIdId, bucketInventoryIdPutBucketInventoryConfigurationBodyParam, bucketInventoryIdInventory, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketInventoryId', 'putBucketInventoryConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketInventoryConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketInventoryConfiguration(bucketInventoryIdBucket, bucketInventoryIdId, bucketInventoryIdInventory, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.InventoryConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketInventoryId', 'getBucketInventoryConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketLifecycleBucket = 'fakedata';
    const bucketLifecycleLifecycle = true;
    const bucketLifecyclePutBucketLifecycleConfigurationBodyParam = {
      LifecycleConfiguration: {}
    };
    describe('#putBucketLifecycleConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketLifecycleConfiguration(bucketLifecycleBucket, bucketLifecyclePutBucketLifecycleConfigurationBodyParam, bucketLifecycleLifecycle, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLifecycle', 'putBucketLifecycleConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketLifecycleConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketLifecycleConfiguration(bucketLifecycleBucket, bucketLifecycleLifecycle, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Rules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLifecycle', 'getBucketLifecycleConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketMetricsIdBucket = 'fakedata';
    const bucketMetricsIdId = 'fakedata';
    const bucketMetricsIdMetrics = true;
    const bucketMetricsIdPutBucketMetricsConfigurationBodyParam = {
      MetricsConfiguration: {}
    };
    describe('#putBucketMetricsConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketMetricsConfiguration(bucketMetricsIdBucket, bucketMetricsIdId, bucketMetricsIdPutBucketMetricsConfigurationBodyParam, bucketMetricsIdMetrics, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketMetricsId', 'putBucketMetricsConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketMetricsConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketMetricsConfiguration(bucketMetricsIdBucket, bucketMetricsIdId, bucketMetricsIdMetrics, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.MetricsConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketMetricsId', 'getBucketMetricsConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketPolicyBucket = 'fakedata';
    const bucketPolicyPolicy = true;
    const bucketPolicyPutBucketPolicyBodyParam = {
      Policy: 'string'
    };
    describe('#putBucketPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketPolicy(bucketPolicyBucket, bucketPolicyPutBucketPolicyBodyParam, bucketPolicyPolicy, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPolicy', 'putBucketPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketPolicy(bucketPolicyBucket, bucketPolicyPolicy, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.Policy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPolicy', 'getBucketPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketReplicationBucket = 'fakedata';
    const bucketReplicationReplication = true;
    const bucketReplicationPutBucketReplicationBodyParam = {
      ReplicationConfiguration: {}
    };
    describe('#putBucketReplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketReplication(bucketReplicationBucket, bucketReplicationPutBucketReplicationBodyParam, bucketReplicationReplication, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketReplication', 'putBucketReplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketReplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketReplication(bucketReplicationBucket, bucketReplicationReplication, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.ReplicationConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketReplication', 'getBucketReplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketTaggingBucket = 'fakedata';
    const bucketTaggingTagging = true;
    const bucketTaggingPutBucketTaggingBodyParam = {
      Tagging: {}
    };
    describe('#putBucketTagging - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketTagging(bucketTaggingBucket, bucketTaggingPutBucketTaggingBodyParam, bucketTaggingTagging, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketTagging', 'putBucketTagging', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketTagging - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketTagging(bucketTaggingBucket, bucketTaggingTagging, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.TagSet));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketTagging', 'getBucketTagging', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketWebsiteBucket = 'fakedata';
    const bucketWebsiteWebsite = true;
    const bucketWebsitePutBucketWebsiteBodyParam = {
      WebsiteConfiguration: {}
    };
    describe('#putBucketWebsite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketWebsite(bucketWebsiteBucket, bucketWebsitePutBucketWebsiteBodyParam, bucketWebsiteWebsite, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketWebsite', 'putBucketWebsite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketWebsite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketWebsite(bucketWebsiteBucket, bucketWebsiteWebsite, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.RedirectAllRequestsTo);
                assert.equal('object', typeof data.response.IndexDocument);
                assert.equal('object', typeof data.response.ErrorDocument);
                assert.equal(true, Array.isArray(data.response.RoutingRules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketWebsite', 'getBucketWebsite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketDeleteBucket = 'fakedata';
    const bucketDeleteDeleteParam = true;
    const bucketDeleteDeleteObjectsBodyParam = {
      Delete: {}
    };
    describe('#deleteObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteObjects(bucketDeleteBucket, bucketDeleteDeleteObjectsBodyParam, bucketDeleteDeleteParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Deleted));
                assert.equal(true, Array.isArray(data.response.Errors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketDelete', 'deleteObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketPublicAccessBlockBucket = 'fakedata';
    const bucketPublicAccessBlockPublicAccessBlock = true;
    const bucketPublicAccessBlockPutPublicAccessBlockBodyParam = {
      PublicAccessBlockConfiguration: {}
    };
    describe('#putPublicAccessBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putPublicAccessBlock(bucketPublicAccessBlockBucket, bucketPublicAccessBlockPutPublicAccessBlockBodyParam, bucketPublicAccessBlockPublicAccessBlock, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPublicAccessBlock', 'putPublicAccessBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPublicAccessBlock - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPublicAccessBlock(bucketPublicAccessBlockBucket, bucketPublicAccessBlockPublicAccessBlock, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.PublicAccessBlockConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPublicAccessBlock', 'getPublicAccessBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketAccelerateBucket = 'fakedata';
    const bucketAccelerateAccelerate = true;
    const bucketAcceleratePutBucketAccelerateConfigurationBodyParam = {
      AccelerateConfiguration: {}
    };
    describe('#putBucketAccelerateConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketAccelerateConfiguration(bucketAccelerateBucket, bucketAcceleratePutBucketAccelerateConfigurationBodyParam, bucketAccelerateAccelerate, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAccelerate', 'putBucketAccelerateConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketAccelerateConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketAccelerateConfiguration(bucketAccelerateBucket, bucketAccelerateAccelerate, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('Suspended', data.response.Status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAccelerate', 'getBucketAccelerateConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketAclBucket = 'fakedata';
    const bucketAclAcl = true;
    const bucketAclPutBucketAclBodyParam = {
      AccessControlPolicy: {
        Grants: [
          {
            Grantee: {
              DisplayName: 'string',
              EmailAddress: 'string',
              ID: 'string',
              Type: 'CanonicalUser',
              URI: 'string'
            },
            Permission: 'READ'
          }
        ],
        Owner: {
          DisplayName: 'string',
          ID: 'string'
        }
      }
    };
    describe('#putBucketAcl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketAcl(bucketAclBucket, bucketAclPutBucketAclBodyParam, bucketAclAcl, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAcl', 'putBucketAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketAcl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketAcl(bucketAclBucket, bucketAclAcl, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.Owner);
                assert.equal(true, Array.isArray(data.response.Grants));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAcl', 'getBucketAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketLifecycleDeprecatedBucket = 'fakedata';
    const bucketLifecycleDeprecatedLifecycle = true;
    const bucketLifecycleDeprecatedPutBucketLifecycleBodyParam = {
      LifecycleConfiguration: {}
    };
    describe('#putBucketLifecycle - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketLifecycle(bucketLifecycleDeprecatedBucket, bucketLifecycleDeprecatedPutBucketLifecycleBodyParam, bucketLifecycleDeprecatedLifecycle, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLifecycleDeprecated', 'putBucketLifecycle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketLifecycle - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketLifecycle(bucketLifecycleDeprecatedBucket, bucketLifecycleDeprecatedLifecycle, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Rules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLifecycleDeprecated', 'getBucketLifecycle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketLocationBucket = 'fakedata';
    const bucketLocationLocation = true;
    describe('#getBucketLocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketLocation(bucketLocationBucket, bucketLocationLocation, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('eu-central-1', data.response.LocationConstraint);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLocation', 'getBucketLocation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketLoggingBucket = 'fakedata';
    const bucketLoggingLogging = true;
    const bucketLoggingPutBucketLoggingBodyParam = {
      BucketLoggingStatus: {}
    };
    describe('#putBucketLogging - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketLogging(bucketLoggingBucket, bucketLoggingPutBucketLoggingBodyParam, bucketLoggingLogging, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLogging', 'putBucketLogging', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketLogging - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketLogging(bucketLoggingBucket, bucketLoggingLogging, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.LoggingEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLogging', 'getBucketLogging', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketNotificationBucket = 'fakedata';
    const bucketNotificationNotification = true;
    const bucketNotificationPutBucketNotificationConfigurationBodyParam = {
      NotificationConfiguration: {}
    };
    describe('#putBucketNotificationConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketNotificationConfiguration(bucketNotificationBucket, bucketNotificationPutBucketNotificationConfigurationBodyParam, bucketNotificationNotification, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketNotification', 'putBucketNotificationConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketNotificationConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketNotificationConfiguration(bucketNotificationBucket, bucketNotificationNotification, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.TopicConfigurations));
                assert.equal(true, Array.isArray(data.response.QueueConfigurations));
                assert.equal(true, Array.isArray(data.response.LambdaFunctionConfigurations));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketNotification', 'getBucketNotificationConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketNotificationDeprecatedBucket = 'fakedata';
    const bucketNotificationDeprecatedNotification = true;
    const bucketNotificationDeprecatedPutBucketNotificationBodyParam = {
      NotificationConfiguration: {
        QueueConfiguration: {
          Event: 's3:ObjectCreated:Put',
          Events: [
            's3:ObjectCreated:Put'
          ],
          Id: 'MDQ2OGQ4NDEtOTBmNi00YTM4LTk0NzYtZDIwN2I3NWQ1NjIx',
          Queue: 'arn:aws:sqs:us-east-1:acct-id:S3ObjectCreatedEventQueue'
        },
        TopicConfiguration: {
          Event: 's3:ObjectCreated:Copy',
          Events: [
            's3:ObjectCreated:Copy'
          ],
          Id: 'YTVkMWEzZGUtNTY1NS00ZmE2LWJjYjktMmRlY2QwODFkNTJi',
          Topic: 'arn:aws:sns:us-east-1:acct-id:S3ObjectCreatedEventTopic'
        }
      }
    };
    describe('#putBucketNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketNotification(bucketNotificationDeprecatedBucket, bucketNotificationDeprecatedPutBucketNotificationBodyParam, bucketNotificationDeprecatedNotification, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketNotificationDeprecated', 'putBucketNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketNotification(bucketNotificationDeprecatedBucket, bucketNotificationDeprecatedNotification, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.TopicConfiguration);
                assert.equal('object', typeof data.response.QueueConfiguration);
                assert.equal('object', typeof data.response.CloudFunctionConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketNotificationDeprecated', 'getBucketNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketPolicyStatusBucket = 'fakedata';
    const bucketPolicyStatusPolicyStatus = true;
    describe('#getBucketPolicyStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketPolicyStatus(bucketPolicyStatusBucket, bucketPolicyStatusPolicyStatus, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.PolicyStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPolicyStatus', 'getBucketPolicyStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketRequestPaymentBucket = 'fakedata';
    const bucketRequestPaymentRequestPayment = true;
    const bucketRequestPaymentPutBucketRequestPaymentBodyParam = {
      RequestPaymentConfiguration: {}
    };
    describe('#putBucketRequestPayment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketRequestPayment(bucketRequestPaymentBucket, bucketRequestPaymentPutBucketRequestPaymentBodyParam, bucketRequestPaymentRequestPayment, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketRequestPayment', 'putBucketRequestPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketRequestPayment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketRequestPayment(bucketRequestPaymentBucket, bucketRequestPaymentRequestPayment, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('BucketOwner', data.response.Payer);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketRequestPayment', 'getBucketRequestPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketVersioningBucket = 'fakedata';
    const bucketVersioningVersioning = true;
    const bucketVersioningPutBucketVersioningBodyParam = {
      VersioningConfiguration: {}
    };
    describe('#putBucketVersioning - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBucketVersioning(bucketVersioningBucket, bucketVersioningPutBucketVersioningBodyParam, bucketVersioningVersioning, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketVersioning', 'putBucketVersioning', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBucketVersioning - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBucketVersioning(bucketVersioningBucket, bucketVersioningVersioning, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('Suspended', data.response.Status);
                assert.equal('Enabled', data.response.MFADelete);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketVersioning', 'getBucketVersioning', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketObjectLockBucket = 'fakedata';
    const bucketObjectLockObjectLock = true;
    const bucketObjectLockPutObjectLockConfigurationBodyParam = {
      ObjectLockConfiguration: {
        ObjectLockEnabled: 'Enabled',
        Rule: {
          DefaultRetention: {
            Mode: 'GOVERNANCE',
            Days: 4,
            Years: 10
          }
        }
      }
    };
    describe('#putObjectLockConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putObjectLockConfiguration(bucketObjectLockBucket, bucketObjectLockPutObjectLockConfigurationBodyParam, bucketObjectLockObjectLock, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketObjectLock', 'putObjectLockConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObjectLockConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getObjectLockConfiguration(bucketObjectLockBucket, bucketObjectLockObjectLock, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.ObjectLockConfiguration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketObjectLock', 'getObjectLockConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketAnalyticsBucket = 'fakedata';
    const bucketAnalyticsAnalytics = true;
    describe('#listBucketAnalyticsConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listBucketAnalyticsConfigurations(bucketAnalyticsBucket, null, bucketAnalyticsAnalytics, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.IsTruncated);
                assert.equal('string', data.response.ContinuationToken);
                assert.equal('string', data.response.NextContinuationToken);
                assert.equal(true, Array.isArray(data.response.AnalyticsConfigurationList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAnalytics', 'listBucketAnalyticsConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketInventoryBucket = 'fakedata';
    const bucketInventoryInventory = true;
    describe('#listBucketInventoryConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listBucketInventoryConfigurations(bucketInventoryBucket, null, bucketInventoryInventory, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.ContinuationToken);
                assert.equal(true, Array.isArray(data.response.InventoryConfigurationList));
                assert.equal(true, data.response.IsTruncated);
                assert.equal('string', data.response.NextContinuationToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketInventory', 'listBucketInventoryConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketMetricsBucket = 'fakedata';
    const bucketMetricsMetrics = true;
    describe('#listBucketMetricsConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listBucketMetricsConfigurations(bucketMetricsBucket, null, bucketMetricsMetrics, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.IsTruncated);
                assert.equal('string', data.response.ContinuationToken);
                assert.equal('string', data.response.NextContinuationToken);
                assert.equal(true, Array.isArray(data.response.MetricsConfigurationList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketMetrics', 'listBucketMetricsConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBuckets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listBuckets((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.Buckets));
                assert.equal('object', typeof data.response.Owner);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Root', 'listBuckets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketUploadsUploads = true;
    describe('#listMultipartUploads - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listMultipartUploads('fakedata', null, null, null, null, null, null, null, null, null, bucketUploadsUploads, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.bucket);
                assert.equal('string', data.response.keyMarkerQuery);
                assert.equal('string', data.response.uploadIdMarkerQuery);
                assert.equal('string', data.response.NextKeyMarker);
                assert.equal('string', data.response.Prefix);
                assert.equal('string', data.response.Delimiter);
                assert.equal('string', data.response.NextUploadIdMarker);
                assert.equal(8, data.response.maxUploadsQuery);
                assert.equal(true, data.response.IsTruncated);
                assert.equal(true, Array.isArray(data.response.Uploads));
                assert.equal(true, Array.isArray(data.response.CommonPrefixes));
                assert.equal('url', data.response.EncodingType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketUploads', 'listMultipartUploads', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketVersionsBucket = 'fakedata';
    const bucketVersionsVersions = true;
    describe('#listObjectVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listObjectVersions(bucketVersionsBucket, null, null, null, null, null, null, null, null, null, bucketVersionsVersions, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.IsTruncated);
                assert.equal('string', data.response.keyMarkerQuery);
                assert.equal('string', data.response.versionIdMarkerQuery);
                assert.equal('string', data.response.NextKeyMarker);
                assert.equal('string', data.response.NextVersionIdMarker);
                assert.equal(true, Array.isArray(data.response.Versions));
                assert.equal(true, Array.isArray(data.response.DeleteMarkers));
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Prefix);
                assert.equal('string', data.response.Delimiter);
                assert.equal(5, data.response.maxKeysQuery);
                assert.equal(true, Array.isArray(data.response.CommonPrefixes));
                assert.equal('url', data.response.EncodingType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketVersions', 'listObjectVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bucketListType2Bucket = 'fakedata';
    const bucketListType2ListType = 'fakedata';
    describe('#listObjectsV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listObjectsV2(bucketListType2Bucket, null, null, null, null, null, null, null, null, null, bucketListType2ListType, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.IsTruncated);
                assert.equal(true, Array.isArray(data.response.Contents));
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Prefix);
                assert.equal('string', data.response.Delimiter);
                assert.equal(9, data.response.maxKeysQuery);
                assert.equal(true, Array.isArray(data.response.CommonPrefixes));
                assert.equal('url', data.response.EncodingType);
                assert.equal(8, data.response.KeyCount);
                assert.equal('string', data.response.continuationTokenQuery);
                assert.equal('string', data.response.NextContinuationToken);
                assert.equal('string', data.response.StartAfter);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketListType2', 'listObjectsV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putObjectNewSTSRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putObjectNewSTSRole(null, 'bucket', 'key', 'body', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'putObjectNewSTSRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucket - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucket(bucketBucket, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'deleteBucket', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#abortMultipartUpload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.abortMultipartUpload(bucketBucket, bucketKey, bucketUploadId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bucket', 'abortMultipartUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketAnalyticsConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketAnalyticsConfiguration(bucketAnalyticsIdBucket, bucketAnalyticsIdId, bucketAnalyticsIdAnalytics, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketAnalyticsId', 'deleteBucketAnalyticsConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketCors - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketCors(bucketCorsBucket, bucketCorsCors, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketCors', 'deleteBucketCors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketEncryption - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketEncryption(bucketEncryptionBucket, bucketEncryptionEncryption, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketEncryption', 'deleteBucketEncryption', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketInventoryConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketInventoryConfiguration(bucketInventoryIdBucket, bucketInventoryIdId, bucketInventoryIdInventory, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketInventoryId', 'deleteBucketInventoryConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketLifecycle - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketLifecycle(bucketLifecycleBucket, bucketLifecycleLifecycle, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketLifecycle', 'deleteBucketLifecycle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketMetricsConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketMetricsConfiguration(bucketMetricsIdBucket, bucketMetricsIdId, bucketMetricsIdMetrics, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketMetricsId', 'deleteBucketMetricsConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketPolicy(bucketPolicyBucket, bucketPolicyPolicy, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPolicy', 'deleteBucketPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketReplication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketReplication(bucketReplicationBucket, bucketReplicationReplication, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketReplication', 'deleteBucketReplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketTagging - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketTagging(bucketTaggingBucket, bucketTaggingTagging, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketTagging', 'deleteBucketTagging', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBucketWebsite - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBucketWebsite(bucketWebsiteBucket, bucketWebsiteWebsite, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketWebsite', 'deleteBucketWebsite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePublicAccessBlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePublicAccessBlock(bucketPublicAccessBlockBucket, bucketPublicAccessBlockPublicAccessBlock, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_s3-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BucketPublicAccessBlock', 'deletePublicAccessBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
