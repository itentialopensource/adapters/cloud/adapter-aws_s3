## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls
These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for AWS S3. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for AWS S3.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Amazon AWS S3. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listPartsSTSRole(bucket, key, maxParts, partNumberMarker, uploadId, maxPartsQuery, partNumberMarkerQuery, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the parts that have been uploaded for a specific multipart upload.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">completeMultipartUploadSTSRole(bucket, key, uploadId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Completes a multipart upload by assembling previously uploaded parts.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">abortMultipartUploadSTSRole(bucket, key, uploadId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Aborts a multipart upload.   To verify that all parts have been removed, so you don't get charged for the part storage, you should call the List Parts operation and ensure the parts list is empty.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copyObjectSTSRole(bucket, key, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a copy of an object that is already stored in Amazon S3.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listObjectsSTSRole(bucket, delimiter, encodingType = 'url', marker, maxKeys, prefix, maxKeysQuery, markerQuery, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns some or all (up to 1000) of the objects in a bucket. You can use the request parameters as selection criteria to return a subset of the objects in a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBucketSTSRole(bucket, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a new bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketSTSRole(bucket, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the bucket. All objects (including all object versions and Delete Markers) in the bucket must be deleted before the bucket itself can be deleted.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headBucketSTSRole(bucket, stsParams, roleName, callback)</td>
    <td style="padding:15px">This operation is useful to determine if a bucket exists and you have permission to access it.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMultipartUploadSTSRole(bucket, key, body, uploads = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Initiates a multipart upload and returns an upload ID.     Note:  After you initiate multipart upload and upload one or more parts, you must either complete or abort multipart upload in order to stop getting charged for storage of the uploaded parts. Only after you either complete or abort multipart upload, Amazon S3 frees up the parts storage and stops charging you for the parts storage.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectSTSRole(bucket, key, responseCacheControl, responseContentDisposition, responseContentEncoding, responseContentLanguage, responseContentType, responseExpires, versionId, partNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves objects from Amazon S3.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectNewSTSRole(region, bucket, key, responseCacheControl, responseContentDisposition, responseContentEncoding, responseContentLanguage, responseContentType, responseExpires, versionId, partNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves objects from Amazon S3.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectSTSRole(bucket, key, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds an object to a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectNewSTSRole(region, bucket, key, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds an object to a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectSTSRole(bucket, key, versionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes the null version (if there is one) of an object and inserts a delete marker, which becomes the latest version of the object. If there isn't a null version, Amazon S3 does not remove any objects.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headObjectSTSRole(bucket, key, versionId, partNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">The HEAD operation retrieves metadata from an object without returning the object itself. This operation is useful if you're only interested in an object's metadata. To use HEAD, you must have READ access to the object.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectTaggingSTSRole(bucket, key, versionId, tagging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the tag-set of an object.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectTaggingSTSRole(bucket, key, versionId, body, tagging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the supplied tag-set to an object that already exists in a bucket</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectTaggingSTSRole(bucket, key, versionId, tagging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes the tag-set from an existing object.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectAclSTSRole(bucket, key, versionId, acl = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the access control list (ACL) of an object.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectAclSTSRole(bucket, key, versionId, body, acl = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">uses the acl subresource to set the access control list (ACL) permissions for an object that already exists in a bucket</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectLegalHoldSTSRole(bucket, key, versionId, legalHold = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets an object's current Legal Hold status.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectLegalHoldSTSRole(bucket, key, versionId, body, legalHold = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Applies a Legal Hold configuration to the specified object.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectRetentionSTSRole(bucket, key, versionId, retention = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves an object's retention settings.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectRetentionSTSRole(bucket, key, versionId, body, retention = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Places an Object Retention configuration on an object.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectTorrentSTSRole(bucket, key, torrent = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Return torrent files from a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreObjectSTSRole(bucket, key, versionId, body, restore = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Restores an archived copy of an object back into Amazon S3</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">selectObjectContentSTSRole(bucket, key, body, select = 'true', selectType = '2', stsParams, roleName, callback)</td>
    <td style="padding:15px">This operation filters the contents of an Amazon S3 object based on a simple Structured Query Language (SQL) statement. In the request, along with the SQL expression, you must also specify a data serialization format (JSON or CSV) of the object. Amazon S3 uses this to parse object data into records, and returns only records that match the specified SQL expression. You must also specify the data serialization format for the response.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPartSTSRole(bucket, key, partNumber, uploadId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Uploads a part in a multipart upload.     Note:  After you initiate multipart upload and upload one or more parts, you must either complete or abort multipart upload in order to stop getting charged for storage of the uploaded parts. Only after you either complete or abort multipart upload, Amazon S3 frees up the parts storage and stops charging you for the parts storage.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPartCopySTSRole(bucket, key, partNumber, uploadId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Uploads a part by copying data from an existing object as data source.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketAnalyticsConfigurationSTSRole(bucket, id, analytics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets an analytics configuration for the bucket (specified by the analytics configuration ID).</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketAnalyticsConfigurationSTSRole(bucket, id, body, analytics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets an analytics configuration for the bucket (specified by the analytics configuration ID).</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketAnalyticsConfigurationSTSRole(bucket, id, analytics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an analytics configuration for the bucket (specified by the analytics configuration ID).   To use this operation, you must have permissions to perform the s3:PutAnalyticsConfiguration action. The bucket owner has this permission by default. The bucket owner can grant this permission to others.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketCorsSTSRole(bucket, cors = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the CORS configuration for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketCorsSTSRole(bucket, body, cors = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the CORS configuration for a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketCorsSTSRole(bucket, cors = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the CORS configuration information set for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketEncryptionSTSRole(bucket, encryption = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the server-side encryption configuration of a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketEncryptionSTSRole(bucket, body, encryption = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a new server-side encryption configuration (or replaces an existing one, if present).</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketEncryptionSTSRole(bucket, encryption = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the server-side encryption configuration from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketInventoryConfigurationSTSRole(bucket, id, inventory = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns an inventory configuration (identified by the inventory ID) from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketInventoryConfigurationSTSRole(bucket, id, body, inventory = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds an inventory configuration (identified by the inventory ID) from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketInventoryConfigurationSTSRole(bucket, id, inventory = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an inventory configuration (identified by the inventory ID) from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketLifecycleConfigurationSTSRole(bucket, lifecycle = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the lifecycle configuration information set on the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketLifecycleConfigurationSTSRole(bucket, body, lifecycle = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets lifecycle configuration for your bucket. If a lifecycle configuration exists, it replaces it.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketLifecycleSTSRole(bucket, lifecycle = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the lifecycle configuration from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketMetricsConfigurationSTSRole(bucket, id, metrics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets a metrics configuration (specified by the metrics configuration ID) from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketMetricsConfigurationSTSRole(bucket, id, body, metrics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets a metrics configuration (specified by the metrics configuration ID) for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketMetricsConfigurationSTSRole(bucket, id, metrics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a metrics configuration (specified by the metrics configuration ID) from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketPolicySTSRole(bucket, policy = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the policy of a specified bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketPolicySTSRole(bucket, body, policy = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Applies an Amazon S3 bucket policy to an Amazon S3 bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketPolicySTSRole(bucket, policy = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the policy from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketReplicationSTSRole(bucket, replication = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the replication configuration of a bucket.      It can take a while to propagate the put or delete a replication configuration to all Amazon S3 systems. Therefore, a get request soon after put or delete can return a wrong result.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketReplicationSTSRole(bucket, body, replication = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a replication configuration or replaces an existing one. For more information, see  Cross-Region Replication (CRR)  in the  Amazon S3 Developer Guide .</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketReplicationSTSRole(bucket, replication = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the replication configuration from the bucket. For information about replication configuration, see  Cross-Region Replication (CRR)  in the  Amazon S3 Developer Guide .</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketTaggingSTSRole(bucket, tagging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the tag set associated with the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketTaggingSTSRole(bucket, body, tagging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the tags for a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketTaggingSTSRole(bucket, tagging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the tags from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketWebsiteSTSRole(bucket, website = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the website configuration for a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketWebsiteSTSRole(bucket, body, website = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Set the website configuration for a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBucketWebsiteSTSRole(bucket, website = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">This operation removes the website configuration from the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectsSTSRole(bucket, body, deleteParam = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">This operation enables you to delete multiple objects from a bucket using a single HTTP request. You may specify up to 1000 keys.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPublicAccessBlockSTSRole(bucket, publicAccessBlock = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves the  PublicAccessBlock  configuration for an Amazon S3 bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPublicAccessBlockSTSRole(bucket, body, publicAccessBlock = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates or modifies the  PublicAccessBlock  configuration for an Amazon S3 bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePublicAccessBlockSTSRole(bucket, publicAccessBlock = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes the  PublicAccessBlock  configuration from an Amazon S3 bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketAccelerateConfigurationSTSRole(bucket, accelerate = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the accelerate configuration of a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketAccelerateConfigurationSTSRole(bucket, body, accelerate = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the accelerate configuration of an existing bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketAclSTSRole(bucket, acl = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the access control policy for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketAclSTSRole(bucket, body, acl = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the permissions on a bucket using access control lists (ACL).</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketLifecycleSTSRole(bucket, lifecycle = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">No longer used, see the GetBucketLifecycleConfiguration operation.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketLifecycleSTSRole(bucket, body, lifecycle = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">No longer used, see the PutBucketLifecycleConfiguration operation.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketLocationSTSRole(bucket, location = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the region the bucket resides in.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketLoggingSTSRole(bucket, logging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the logging status of a bucket and the permissions users have to view and modify that status. To use GET, you must be the bucket owner.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketLoggingSTSRole(bucket, body, logging = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Set the logging parameters for a bucket and to specify permissions for who can view and modify the logging parameters. To set the logging status of a bucket, you must be the bucket owner.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketNotificationConfigurationSTSRole(bucket, notification = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the notification configuration of a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketNotificationConfigurationSTSRole(bucket, body, notification = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables notifications of specified events for a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketNotificationSTSRole(bucket, notification = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">No longer used, see the GetBucketNotificationConfiguration operation.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketNotificationSTSRole(bucket, body, notification = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">No longer used, see the PutBucketNotificationConfiguration operation.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketPolicyStatusSTSRole(bucket, policyStatus = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves the policy status for an Amazon S3 bucket, indicating whether the bucket is public.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketRequestPaymentSTSRole(bucket, requestPayment = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the request payment configuration of a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketRequestPaymentSTSRole(bucket, body, requestPayment = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the request payment configuration for a bucket. By default, the bucket owner pays for downloads from the bucket. This configuration parameter enables the bucket owner (only) to specify that the person requesting the download will be charged for the download. Documentation on requester pays buckets can be found at http://docs.aws.amazon.com/AmazonS3/latest/dev/RequesterPaysBuckets.html</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBucketVersioningSTSRole(bucket, versioning = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns the versioning state of a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBucketVersioningSTSRole(bucket, body, versioning = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Sets the versioning state of an existing bucket. To set the versioning state, you must be the bucket owner.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectLockConfigurationSTSRole(bucket, objectLock = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the object lock configuration for a bucket. The rule specified in the object lock configuration will be applied by default to every new object placed in the specified bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putObjectLockConfigurationSTSRole(bucket, body, objectLock = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Places an object lock configuration on the specified bucket. The rule specified in the object lock configuration will be applied by default to every new object placed in the specified bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBucketAnalyticsConfigurationsSTSRole(bucket, continuationToken, analytics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the analytics configurations for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBucketInventoryConfigurationsSTSRole(bucket, continuationToken, inventory = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns a list of inventory configurations for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBucketMetricsConfigurationsSTSRole(bucket, continuationToken, metrics = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the metrics configurations for the bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBucketsSTSRole(stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns a list of all buckets owned by the authenticated sender of the request.</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMultipartUploadsSTSRole(bucket, delimiter, encodingType = 'url', keyMarker, maxUploads, prefix, uploadIdMarker, maxUploadsQuery, keyMarkerQuery, uploadIdMarkerQuery, uploads = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">This operation lists in-progress multipart uploads.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listObjectVersionsSTSRole(bucket, delimiter, encodingType = 'url', keyMarker, maxKeys, prefix, versionIdMarker, maxKeysQuery, keyMarkerQuery, versionIdMarkerQuery, versions = 'true', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns metadata about all of the versions of objects in a bucket.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listObjectsV2STSRole(bucket, delimiter, encodingType = 'url', maxKeys, prefix, continuationToken, fetchOwner, startAfter, maxKeysQuery, continuationTokenQuery, listType = '2', stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns some or all (up to 1000) of the objects in a bucket. You can use the request parameters as selection criteria to return a subset of the objects in a bucket. Note: ListObjectsV2 is the revised List Objects API and we recommend you use this revised API for new application development.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
