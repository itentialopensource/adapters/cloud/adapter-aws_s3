# Amazon AWS S3

Vendor: Amazon AWS
Homepage: https://aws.amazon.com/s3/

Product: Amazon AWS S3
Product Page: https://aws.amazon.com/s3/

## Introduction
We classify AWS S3 into the Cloud domain as S3 provides Cloud storage services.

"S3 is built to retrieve any amount of data from anywhere."
"S3 is an object storage service offering industry-leading scalability, data availability, security, and performance."  

The AWS S3 adapter can be integrated to the Itential Device Broker which will allow your VPCs to be managed within the Itential Configuration Manager Application.

## Why Integrate
The AWS S3 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS S3. With this adapter you have the ability to perform operations such as:

- Backup and restore data. 
- Configure and Manage access constrols.
- Configure compliance requirements.


## Additional Product Documentation
The [API documents for AWS S3](https://docs.aws.amazon.com/AmazonS3/latest/API/Welcome.html)